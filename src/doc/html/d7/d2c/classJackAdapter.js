var classJackAdapter =
[
    [ "bufferSize", "d7/d2c/classJackAdapter.html#a6a957ded47affe20ca51e98f87124a42", null ],
    [ "connectToServer", "d7/d2c/classJackAdapter.html#a221a1ac7a60ab38558de730c64428af7", null ],
    [ "cpuLoad", "d7/d2c/classJackAdapter.html#afa5994297183b374b8a01e6fa683a654", null ],
    [ "error", "d7/d2c/classJackAdapter.html#a55123c4a317c8c6c2a186b6a66867902", null ],
    [ "instance", "d7/d2c/classJackAdapter.html#a431136befd01cac7ecbdf19099075b4d", null ],
    [ "registerStereoInputPort", "d7/d2c/classJackAdapter.html#a675123d2fa6f327d18985fecf305bb56", null ],
    [ "registerStereoOutputPort", "d7/d2c/classJackAdapter.html#a3cbec70a8bbaecb9f4d1fbdbdbf5a4fd", null ],
    [ "sampleRate", "d7/d2c/classJackAdapter.html#ab66793a663a251a706f4c7b4843fed88", null ],
    [ "setProcessor", "d7/d2c/classJackAdapter.html#a69d25a5529343586d8c8f3c2b5b32be1", null ],
    [ "startAudioProcessing", "d7/d2c/classJackAdapter.html#ada1468e1b1b4f853d017c480e6b989a4", null ],
    [ "stereoInputPort", "d7/d2c/classJackAdapter.html#a0f52179d6826823348d4b336d40aa71e", null ],
    [ "stereoOutputPort", "d7/d2c/classJackAdapter.html#a4b480c31bfe78d7e64b342014aacab2c", null ],
    [ "stopAudioProcessing", "d7/d2c/classJackAdapter.html#aa8bdeea946ee8d2e67920e7eb355d74e", null ]
];