var classDigitalEqualizer =
[
    [ "DigitalEqualizer", "de/d28/classDigitalEqualizer.html#ae73196a702a995b74874c229f0ac11de", null ],
    [ "~DigitalEqualizer", "de/d28/classDigitalEqualizer.html#a2b5c4ba64b279d781c76a05424f34ab8", null ],
    [ "acquireControls", "de/d28/classDigitalEqualizer.html#a8dd84de986b4a50170f2a296342f55da", null ],
    [ "controls", "de/d28/classDigitalEqualizer.html#a1328730773d4415bf8ad010e0dce0ec1", null ],
    [ "generateFilter", "de/d28/classDigitalEqualizer.html#aa4a96390ed7480d85fbe4fac36239ec2", null ],
    [ "loadControlsFromFile", "de/d28/classDigitalEqualizer.html#a97abb9d7b75b6515f48ffcbc2d1591d1", null ],
    [ "numberOfControls", "de/d28/classDigitalEqualizer.html#a18cb2658f2daba42520eff1afe6e67c3", null ],
    [ "process", "de/d28/classDigitalEqualizer.html#aa65783c000b904e1b937196f9b1f6f4a", null ],
    [ "releaseControls", "de/d28/classDigitalEqualizer.html#a38314bc1535c71f2a20c179f98330d43", null ],
    [ "saveControlsToFile", "de/d28/classDigitalEqualizer.html#a4b878ffb698ba3fea5b36e51328489eb", null ],
    [ "setNumberOfControls", "de/d28/classDigitalEqualizer.html#a1ef9715686cd404bfc0aa95958928be4", null ]
];