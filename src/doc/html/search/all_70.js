var searchData=
[
  ['paintgl',['paintGL',['../d0/d59/classVisualizerWidget.html#a6be9ffb9f7ec557d2ffbd5f04a62ea79',1,'VisualizerWidget']]],
  ['performfft',['performFFT',['../d5/dfd/namespaceFftwAdapter.html#a6e1648d33af4f88a3307e25eee03d945',1,'FftwAdapter']]],
  ['performinversefft',['performInverseFFT',['../d5/dfd/namespaceFftwAdapter.html#ae8e0488a742dd23810797b05a737ebd5',1,'FftwAdapter']]],
  ['pink_5fnoise_5fcombo_5ftext',['PINK_NOISE_COMBO_TEXT',['../d8/dd9/mainwindow_8cpp.html#a12e6cc166c0bb330bc056a35327caac6',1,'mainwindow.cpp']]],
  ['pinknoise',['PinkNoise',['../d2/dd5/classEarProcessor.html#a4f53a396f0e0611b389c7c86456b5bcda47712c41ea770f75eb0c37d095ded8d6',1,'EarProcessor']]],
  ['poly',['poly',['../d6/db0/classPRBSGenerator.html#a19a3b9cb35dbff18b83d34bcafe22e05',1,'PRBSGenerator']]],
  ['prbsgenerator',['PRBSGenerator',['../d6/db0/classPRBSGenerator.html',1,'PRBSGenerator'],['../d6/db0/classPRBSGenerator.html#ac503ed16e187261a8d724a02c231b11f',1,'PRBSGenerator::PRBSGenerator()']]],
  ['prbsgenerator_2eh',['prbsgenerator.h',['../db/d33/prbsgenerator_8h.html',1,'']]],
  ['process',['process',['../de/d28/classDigitalEqualizer.html#aa65783c000b904e1b937196f9b1f6f4a',1,'DigitalEqualizer::process()'],['../d2/dd5/classEarProcessor.html#a2a65b0d2bafc93871789ff7f750d0565',1,'EarProcessor::process()'],['../d1/dd1/classJNoise.html#ae0c1f2035ef7cbfd97e506c41e1e7d06',1,'JNoise::process()'],['../db/d9f/classProcessor.html#a3dad5d3ba8d0946e2f32acecadb1ede5',1,'Processor::process()']]],
  ['processingaudio',['ProcessingAudio',['../d2/dd5/classEarProcessor.html#a094f42adcbeee8e171a2527dae7d1256a9630fd2816910eba9787853679c1d9fc',1,'EarProcessor']]],
  ['processor',['Processor',['../db/d9f/classProcessor.html',1,'Processor'],['../db/d9f/classProcessor.html#a50c89dbf76a073f4fb491628258cf292',1,'Processor::Processor()']]],
  ['processor_2eh',['processor.h',['../d9/d99/processor_8h.html',1,'']]]
];
